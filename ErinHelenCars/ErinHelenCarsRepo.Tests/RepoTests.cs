﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ErinHelenCars.Models;
using ErinHelenCars.Controllers;
using Moq;

namespace ErinHelenCarsRepo.Tests
{

    [TestFixture]
    public class RepoTests
    {
        [Test]
       public void GetAllCars()
        {
            var api = new CarsAPIController();
            var repo = new CarsRepository(new HEEZcarsEntities());
            var result = repo.GetAllCars();
            Assert.IsNotNull(result);
        }


        //Trouble with EF getting this test to work!!!!

        //[Test]
        //public void AddCar()
        //{
        //    var repo = new CarsRepository(new HEEZcarsEntities());
        //    whipz whip = new whipz()
        //    {
        //        vehicleID = 1,
        //        make = "ford",
        //        model = "focus",
        //        year = 2004,
        //        price = 5000,
        //        mileage = 15000
        //    };

        //    repo.AddCar(whip);

        //    Assert.IsNotNullOrEmpty(repo.GetAllCars().ToList().ToString());
        //}

        [Test]
        public void MoqDemo()
        {
            var repo = new Mock<ICarsRepository>();
            repo.Setup(x => x.GetCar(It.IsAny<int>())).Returns(new whipz() { model = "Ford", vehicleID = 3 });

            var myCar = repo.Object.GetCar(3);
        }


    }
}
