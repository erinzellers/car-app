﻿$(document).ready(function () {

    var foo = $("#hello");
    $('#hello').validate({
        rules: {
            make: {
                required: true
            },
            model: {
                required: true
            },
            year: {
                required: true
            },
            mileage: {
                required: true
            },
            adTitle: {
                required: true
            },
            description: {
                required: true
            },
            price: {
                required: true
            },
            pictureURL: {
                required: true
            },
            isAvailable: {
                required: true
            }
        }
    });

    $.validator.setDefaults({
        highlight: function (element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success')
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

});


