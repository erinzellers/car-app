
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ErinHelenCars.Models;

namespace ErinHelenCars.Controllers
{
    public class requestsForInfoesController : Controller
    {
        private HEEZcarsEntities db = new HEEZcarsEntities();

        // GET: requestsForInfoes
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
           
            var requestsForInfoes = db.requestsForInfoes.Include(r => r.whipz);
            return View(requestsForInfoes.ToList());
        }

        // GET: requestsForInfoes/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            requestsForInfo requestsForInfo = db.requestsForInfoes.Find(id);
            if (requestsForInfo == null)
            {
                return HttpNotFound();
            }
            return View(requestsForInfo);
        }

        // GET: requestsForInfoes/Create
        public ActionResult Create()
        {
            ViewBag.vehicleID = new SelectList(db.whipzs, "vehicleID", "make");
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "requestID,vehicleID,name,email,phoneNumber,bestTimeToCall,preferredContactMethod,timeframeToPurchase,additionalInformation,lastContactDate,statusOfRequest")] requestsForInfo requestsForInfo)
        {
            if (ModelState.IsValid)
            {
                db.requestsForInfoes.Add(requestsForInfo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.vehicleID = new SelectList(db.whipzs, "vehicleID", "make", requestsForInfo.vehicleID);
            return View(requestsForInfo);
        }

        // GET: requestsForInfoes/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            requestsForInfo requestsForInfo = db.requestsForInfoes.Find(id);
            if (requestsForInfo == null)
            {
                return HttpNotFound();
            }
            ViewBag.vehicleID = new SelectList(db.whipzs, "vehicleID", "make", requestsForInfo.vehicleID);
            return View(requestsForInfo);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "requestID,vehicleID,name,email,phoneNumber,bestTimeToCall,preferredContactMethod,timeframeToPurchase,additionalInformation,lastContactDate,statusOfRequest")] requestsForInfo requestsForInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(requestsForInfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.vehicleID = new SelectList(db.whipzs, "vehicleID", "make", requestsForInfo.vehicleID);
            return View(requestsForInfo);
        }

        // GET: requestsForInfoes/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            requestsForInfo requestsForInfo = db.requestsForInfoes.Find(id);
            if (requestsForInfo == null)
            {
                return HttpNotFound();
            }
            return View(requestsForInfo);
        }

        // POST: requestsForInfoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            requestsForInfo requestsForInfo = db.requestsForInfoes.Find(id);
            db.requestsForInfoes.Remove(requestsForInfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

//﻿using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.Entity;
//using System.Linq;
//using System.Net;
//using System.Web;
//using System.Web.Mvc;
//using ErinHelenCars.Models;

//namespace ErinHelenCars.Controllers
//{
//    public class requestsForInfoesController : Controller
//    {
//        private HEEZcarsEntities db = new HEEZcarsEntities();

//        // GET: requestsForInfoes
//        public ActionResult Index()
//        {
//            var requestsForInfoes = db.requestsForInfoes.Include(r => r.whipz);
//            return View(requestsForInfoes.ToList());
//        }

//        // GET: requestsForInfoes/Details/5
//        public ActionResult Details(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            requestsForInfo requestsForInfo = db.requestsForInfoes.Find(id);
//            if (requestsForInfo == null)
//            {
//                return HttpNotFound();
//            }
//            return View(requestsForInfo);
//        }

//        // GET: requestsForInfoes/Create
//        public ActionResult Create()
//        {
//            ViewBag.vehicleID = new SelectList(db.whipzs, "vehicleID", "make");
//            return View();
//        }

//        // POST: requestsForInfoes/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "requestID,vehicleID,name,email,phoneNumber,bestTimeToCall,preferredContactMethod,timeframeToPurchase,additionalInformation,lastContactDate,statusOfRequest")] requestsForInfo requestsForInfo)
//        {
//            if (ModelState.IsValid)
//            {
//                db.requestsForInfoes.Add(requestsForInfo);
//                db.SaveChanges();
//                return RedirectToAction("Index");
//            }

//            ViewBag.vehicleID = new SelectList(db.whipzs, "vehicleID", "make", requestsForInfo.vehicleID);
//            return View(requestsForInfo);
//        }

//        // GET: requestsForInfoes/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            requestsForInfo requestsForInfo = db.requestsForInfoes.Find(id);
//            if (requestsForInfo == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.vehicleID = new SelectList(db.whipzs, "vehicleID", "make", requestsForInfo.vehicleID);
//            return View(requestsForInfo);
//        }

//        // POST: requestsForInfoes/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "requestID,vehicleID,name,email,phoneNumber,bestTimeToCall,preferredContactMethod,timeframeToPurchase,additionalInformation,lastContactDate,statusOfRequest")] requestsForInfo requestsForInfo)
//        {
//            if (ModelState.IsValid)
//            {
//                db.Entry(requestsForInfo).State = EntityState.Modified;
//                db.SaveChanges();
//                return RedirectToAction("Index");
//            }
//            ViewBag.vehicleID = new SelectList(db.whipzs, "vehicleID", "make", requestsForInfo.vehicleID);
//            return View(requestsForInfo);
//        }

//        // GET: requestsForInfoes/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            requestsForInfo requestsForInfo = db.requestsForInfoes.Find(id);
//            if (requestsForInfo == null)
//            {
//                return HttpNotFound();
//            }
//            return View(requestsForInfo);
//        }

//        // POST: requestsForInfoes/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            requestsForInfo requestsForInfo = db.requestsForInfoes.Find(id);
//            db.requestsForInfoes.Remove(requestsForInfo);
//            db.SaveChanges();
//            return RedirectToAction("Index");
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                db.Dispose();
//            }
//            base.Dispose(disposing);
//        }
//    }
//}

