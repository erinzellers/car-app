﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ErinHelenCars.Models
{
    public class WhipzMetadata
    {
        public int vehicleID;
        [Display(Name = "Make")]
        public string make;
        [Display(Name = "Model")]
        public string model;
        [Display(Name = "Year")]
        public int year;
        [Display(Name = "Mileage")]
        public int mileage;
        [Display(Name = " ")]
        public string adTitle;
        [Display(Name = "Description")]
        public string description;
        [Display(Name = "Price")]
        public Nullable<int> price;
        [Display(Name = " ")]
        public string URLofPicture;
        [Display(Name = "Available")]
        public bool isAvailable;
    }

    public class RequestsForInfoMetadata
    {
    
        [Display(Name = "Name")]
        public string name;
        [Display(Name = "Email")]
        public string email;
        [Display(Name = "Phone Number")]
        public string phoneNumber;
        [Display(Name = "Best Time to Call")]
        public string bestTimeToCall;
        [Display(Name = "Preferred Method of Contact")]
        public string preferredContactMethod;
        [Display(Name = "How soon are you looking to purchase your new car?")]
        public string timeframeToPurchase;
        [Display(Name = "Additional Comments")]
        public string additionalInformation;
        [Display(Name = "Last Time Contacted")]
        public System.DateTime lastContactDate;
        [Display(Name = "Status")]
        public int statusOfRequest;


    }
}