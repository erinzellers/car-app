﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ErinHelenCars.Models
{
    public class CarsRepository : ICarsRepository
    {
        HEEZcarsEntities ctx;

        public CarsRepository(HEEZcarsEntities entities)
        {
            ctx = entities;
        }

        public void AddCar(whipz p)
        {
            ctx.whipzs.Add(p);
            ctx.SaveChanges();
        }

        public IQueryable<whipz> GetAllCars()
        {
            return ctx.whipzs;
        }


        public List<whipz> GetOrderedListofCars()
        {
            return ctx.whipzs.OrderBy(p => p.make).ThenBy(p => p.model).ToList();
        }


        public whipz GetCar(int? id)
        {
            return ctx.whipzs.First(x => x.vehicleID == id);
        }

        public void DeleteCar(int? id)
        {
            var res = ctx.whipzs.First(x => x.vehicleID == id);

            ctx.whipzs.Remove(res);

        }

    }
}
    