﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ErinHelenCars.Startup))]
namespace ErinHelenCars
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
