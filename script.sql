USE [master]
GO
/****** Object:  Database [HEEZcars]    Script Date: 4/19/2016 3:46:40 PM ******/
CREATE DATABASE [HEEZcars]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'HEEZcars', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\HEEZcars.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'HEEZcars_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\HEEZcars_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [HEEZcars] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [HEEZcars].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [HEEZcars] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [HEEZcars] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [HEEZcars] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [HEEZcars] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [HEEZcars] SET ARITHABORT OFF 
GO
ALTER DATABASE [HEEZcars] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [HEEZcars] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [HEEZcars] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [HEEZcars] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [HEEZcars] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [HEEZcars] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [HEEZcars] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [HEEZcars] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [HEEZcars] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [HEEZcars] SET  DISABLE_BROKER 
GO
ALTER DATABASE [HEEZcars] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [HEEZcars] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [HEEZcars] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [HEEZcars] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [HEEZcars] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [HEEZcars] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [HEEZcars] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [HEEZcars] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [HEEZcars] SET  MULTI_USER 
GO
ALTER DATABASE [HEEZcars] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [HEEZcars] SET DB_CHAINING OFF 
GO
ALTER DATABASE [HEEZcars] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [HEEZcars] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [HEEZcars] SET DELAYED_DURABILITY = DISABLED 
GO
USE [HEEZcars]
GO
/****** Object:  Table [dbo].[requestsForInfo]    Script Date: 4/19/2016 3:46:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[requestsForInfo](
	[requestID] [int] IDENTITY(1,1) NOT NULL,
	[vehicleID] [int] NOT NULL,
	[name] [nvarchar](50) NULL,
	[email] [nvarchar](100) NULL,
	[phoneNumber] [nvarchar](15) NULL,
	[bestTimeToCall] [nvarchar](50) NULL,
	[preferredContactMethod] [nvarchar](50) NULL,
	[timeframeToPurchase] [nvarchar](50) NULL,
	[additionalInformation] [nvarchar](max) NOT NULL,
	[lastContactDate] [datetime] NOT NULL,
	[statusOfRequest] [int] NOT NULL,
 CONSTRAINT [PK_requestsForInfo] PRIMARY KEY CLUSTERED 
(
	[requestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[whipz]    Script Date: 4/19/2016 3:46:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[whipz](
	[vehicleID] [int] IDENTITY(1,1) NOT NULL,
	[make] [nvarchar](50) NOT NULL,
	[model] [nvarchar](50) NOT NULL,
	[year] [int] NOT NULL,
	[mileage] [int] NOT NULL,
	[adTitle] [nvarchar](50) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[price] [money] NOT NULL,
	[URLofPicture] [nvarchar](max) NOT NULL,
	[isAvailable] [bit] NOT NULL,
 CONSTRAINT [PK_whipz] PRIMARY KEY CLUSTERED 
(
	[vehicleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[requestsForInfo] ON 

INSERT [dbo].[requestsForInfo] ([requestID], [vehicleID], [name], [email], [phoneNumber], [bestTimeToCall], [preferredContactMethod], [timeframeToPurchase], [additionalInformation], [lastContactDate], [statusOfRequest]) VALUES (8, 9, N'Erin Zellers', N'EzE@gmail.com', N'502-555-1234', N'anytime', N'email', N'ASAP', N'Hates manual transmission', CAST(N'1900-02-01 00:00:00.000' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[requestsForInfo] OFF
SET IDENTITY_INSERT [dbo].[whipz] ON 

INSERT [dbo].[whipz] ([vehicleID], [make], [model], [year], [mileage], [adTitle], [description], [price], [URLofPicture], [isAvailable]) VALUES (9, N'Toyota', N'Tacoma', 2008, 80000, N'Great Deal!!! One Owner', N'Dark gray grille with black surround, color-keyed power outside mirrors, color-keyed door handles and rear bumper', 16000.0000, N'~/content/images/tacoma.jpg', 1)
INSERT [dbo].[whipz] ([vehicleID], [make], [model], [year], [mileage], [adTitle], [description], [price], [URLofPicture], [isAvailable]) VALUES (10, N'Audi', N'S4', 2012, 45000, N'So Fresh and So Clean', N'PRESTIGE PKG,SILK NAPPA LEATHER SEATING SURFACES,TITANIUM PKG,', 28470.0000, N'~/content/images/s4.jpg', 1)
INSERT [dbo].[whipz] ([vehicleID], [make], [model], [year], [mileage], [adTitle], [description], [price], [URLofPicture], [isAvailable]) VALUES (11, N'Honda', N'Accord', 2003, 110000, N'Definitley not haunted!!', N'Its not full of bees or ghosts.... really its not.', 11000.0000, N'~/content/images/accord.jpg', 1)
INSERT [dbo].[whipz] ([vehicleID], [make], [model], [year], [mileage], [adTitle], [description], [price], [URLofPicture], [isAvailable]) VALUES (12, N'Ford', N'Focus', 2011, 90000, N'Its just beyond that graveyard.', N'This car has a reasonable amount of ghosts in it. Manageable I would say.', 4000.0000, N'~/content/images/accord.jpg', 1)
INSERT [dbo].[whipz] ([vehicleID], [make], [model], [year], [mileage], [adTitle], [description], [price], [URLofPicture], [isAvailable]) VALUES (13, N'Jeep', N'Wrangler', 2014, 41000, N'Back seat, windows up', N'3.6L V6, 4WD, automatic, 2 miles, 20 MPG Hwy, Bright White Clearcoat, stock # K2203G, new', 900.0000, N'~/content/images/jeep.jpg', 1)
SET IDENTITY_INSERT [dbo].[whipz] OFF
ALTER TABLE [dbo].[requestsForInfo]  WITH CHECK ADD  CONSTRAINT [FK_requestsForInfo_whipz1] FOREIGN KEY([vehicleID])
REFERENCES [dbo].[whipz] ([vehicleID])
GO
ALTER TABLE [dbo].[requestsForInfo] CHECK CONSTRAINT [FK_requestsForInfo_whipz1]
GO
USE [master]
GO
ALTER DATABASE [HEEZcars] SET  READ_WRITE 
GO
